import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

  ///////////////////////////////////////////
 //        Operators in Java              //
///////////////////////////////////////////

//        Arithmetic: +, -, *, /, %
//        Comparison: >, <, >=, <=, ==, !=
//        Logical: &&, ||, !
//        Assignment: =

  ///////////////////////////////////////////////////////////////
 //        Conditional Control Structure in Java              //
///////////////////////////////////////////////////////////////

//        Statement allows us to manipulate the flow of the code depending on the evaluation of the condition
//        if and else statement:
        int num1 = 15;
        if(num1 % 5 == 0) {
            System.out.println(num1+" is Divisible by 5");
        }

        num1 = 36;
        if(num1 % 5 == 0){
            System.out.println(num1+" is Divisible by 5");
        } else {
            System.out.println(num1+" is not Divisible by 5");
        }

        Scanner numberScanner = new Scanner(System.in);
//        System.out.println("Please enter an integer number: ");
//        int intNumber = numberScanner.nextInt();
//
//        if(intNumber % 2 == 0){
//            System.out.println(intNumber+" is even!");
//        } else {
//            System.out.println(intNumber+" is odd!");
//        }
  //////////////////////////////////
 //        Switch Cases          //
//////////////////////////////////
        /*
        Switch statements are control flow structures that allow one code block to be run out of many other code blocks.We compare the given value against each cases and if a case matches we will run that code block. This is mostly used if the user is predictable.
        */

//        System.out.println("Enter a number from 1-4 to see an SM malls in one of the four direction:");
//        int directionValue = numberScanner.nextInt();
//        switch (directionValue){
//            case 1:
//                System.out.println("SM North Edsa");
//                break;
//            case 2:
//                System.out.println("SM Southmall");
//                break;
//            case 3:
//                System.out.println("SM City Taytay");
//                break;
//            case 4:
//                System.out.println("SM Manila");
//                break;
//            default:
//                System.out.println("Out of range");
//        }
  //////////////////////////////////////
 //         Java Collections         //
//////////////////////////////////////
        /*
        * Arrays- are object that can contain data. However, in Java, arrays have a fixed/limited number
        * of values with the same data type.
        *
        * Unlike in JS, the length of Java Arrays are established when the array is created:
        *
        * Syntax:
        * dataType[] identifier - new dataType[numberOfElements];
        * dataType[] identifier - {elementA, elementB.....};
        */

        String[] newArr = new String[4];
        newArr[0] = "Clark";
        newArr[1] = "Bruce";
        newArr[2] = "Diana";
        newArr[3] = "Barry";

        /*
        * newArr[4] = 35 : This will cause an error because our newArr only accepts a String value
        *
        * This will show the memory address of our array or the location of our array within the memory
        * System.out.println(newArr);
        */

        /*
        * To Display the actual values of the array, we have to first convert it to a string using the Array
        * class from Java
        *
        *
        * toString() is used to show the values of the array as a string in the terminal
        */
        System.out.println(Arrays.toString(newArr));

        /*
        * toString() - retrieves the actual value of the array as a string
        *
        * Sort Method:
        */
        Arrays.sort(newArr);
        System.out.println("Result of the Arrays.sort()");
        System.out.println(Arrays.toString(newArr));

        Integer[] intArr = new Integer[3];
        intArr [0] = 54;
        intArr [1] = 12;
        intArr [2] = 67;
        System.out.println("Initial Order of the intArr:");
        System.out.println(Arrays.toString(intArr));

        Arrays.sort(intArr);
        System.out.println("Sorted order of the intArr:");
        System.out.println(Arrays.toString(intArr));

        /*
        * binarySearch() method - allows us to pass an argument/item to search for within our array.
        * binarySearch() will then return the index number of the found element.
        * You can use a scanner to get input for your search term.
        */
        String searchTerm = "Bruce";
        int result = Arrays.binarySearch(newArr,searchTerm);
        System.out.println("The index of "+searchTerm+" is: "+result);

        /*
        * Array with initialized Values:
        */
        String[] arrSample = {"Tony","Thor","Steve"};
        System.out.println(Arrays.toString(arrSample));

  ////////////////////////
 //      ArrayList     //
////////////////////////
        /*
        * ArrayList - are resizable collections/array that function similarly to how arrays work in JS.
        *
        * Using the new keyword in creating an ArrayList does not require the data type for the array list
        * to be defined to avoid repetition.
        *
        * Syntax:
        * ArrayList<dataType> identifier = new ArrayList();
        */
        ArrayList<String> students = new ArrayList<>();

  /////////////////////////////////
 //      ArrayList Method       //
/////////////////////////////////
        /*
        * ArrayList Methods
        * arrayListName.add(itemToAdd) - adds elements in our array list:
        */
        students.add("Paul");
        students.add("John");
        System.out.println(students);
        /*
        * arrayListName.get(index) - retrieve items from the array list using its index
        */
        System.out.println(students.get(1));
        /*
        * arrayListName.set(index,value) - update an item by its index
        */
        students.set(0,"George");
        System.out.println(students);
        /*
        * arrayListName.remove(index) - remove an item from the array based on its index.
        */
        students.remove(1);
        System.out.println(students);

        students.add("Ringo");
        /*
        * arrayList.clear() - removes all items from our array list.
        */
        System.out.println(students);
        students.clear();
        System.out.println(students);

        /*
        * arrayListName.size() - gets the length of our array list.
        */
        System.out.println(students.size());
        students.add("James");
        students.add("Wade");
        students.add("Bosh");
        System.out.println(students.size());

        /*
        * ArrayList with initialized values:
        */
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Bill Gates","Elon Musk","Jeff Bezos"));
        System.out.println(employees);
        employees.add("Lucio Tan");
        System.out.println(employees);

  ////////////////////////
 //     Hash Maps      //
////////////////////////
        /*
        * Most Objects in Java are defined and are instantiation of Classes that contain a proper
        * set of properties and methods. There might be use cases where it is not appropriate or you may
        * simply want to store a collection of data in key value pairs.
        *
        * In Java, "keys" also referred as "fields"
        * HashMaps offer flexibility when storing a collection of data.
        *
        * Syntax:
        * HashMap<fieldDataType,valueDataType> identifier = new HashMap();
        */
        HashMap<String,String> userRoles = new HashMap<>();

        /*
        * Add new field and values in the HashMap
        * hashMapName.put(<field>,<values>)
        */
        userRoles.put("Anna","Admin");
        userRoles.put("Alice","User");
        System.out.println(userRoles);

        userRoles.put("Alice","Teacher");
        System.out.println(userRoles);
        userRoles.put("Dennis","User");
        System.out.println(userRoles);

        /*
        * Retrieves values by fields
        * hashMapName.get("field");
        */
        System.out.println(userRoles.get("Alice")); // returns Teacher
        System.out.println(userRoles.get("Dennis")); // returns User
        System.out.println(userRoles.get("Anna")); // returns Admin
        System.out.println(userRoles.get("alice")); // returns null
        System.out.println(userRoles.get("Ellen")); // returns null

        /*
        * removes an element/field-value
        * hashMapName.remove("field");
        */
        userRoles.remove("Dennis");
        System.out.println(userRoles);

        /*
        * retrieve hashMap keys
        * hashMapName.keySet()
        */
        System.out.println(userRoles.keySet());
    }
}