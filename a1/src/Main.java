import java.util.Scanner;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
public class Main {
    public static void main(String[] args) {

  //////////////////////
 //      Array       //
//////////////////////
        String[] fruitArr = {"apple","avocado","banana","kiwi","orange"};
        System.out.println("Fruit in stock: "+Arrays.toString(fruitArr));

        Scanner scanner = new Scanner(System.in);
        System.out.println("Which fruit would you like to get the index of?");
        String fruit = scanner.nextLine();

        int result =Arrays.binarySearch(fruitArr,fruit);
        System.out.println("The index of "+fruit+" is: "+result);

  //////////////////////////
 //      ArrayList       //
//////////////////////////
        ArrayList<String> friends = new ArrayList<>();
        friends.add("Daniel");
        friends.add("Mel");
        friends.add("Jimmy");
        friends.add("Renzo");

        System.out.println("My friends are: "+friends);

  /////////////////////////////
 //        HashMap          //
/////////////////////////////
        HashMap<String,Integer> inventory = new HashMap<>();
        inventory.put("Toothpaste",15);
        inventory.put("Toothbrush",20);
        inventory.put("Soap",12);
        System.out.println("Our current inventory consists of:");
        System.out.println(inventory);
    }
}